# BlockDrop

A falling block game that is nothing like Tetris. (Requires a mouse and 386+)


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## BLKDROP.LSM

<table>
<tr><td>title</td><td>BlockDrop</td></tr>
<tr><td>version</td><td>v0.1</td></tr>
<tr><td>entered&nbsp;date</td><td>2022-02-13</td></tr>
<tr><td>description</td><td>A falling block game</td></tr>
<tr><td>summary</td><td>A falling block game that is nothing like Tetris. (Requires a mouse and 386+)</td></tr>
<tr><td>keywords</td><td>dos game</td></tr>
<tr><td>author</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://gitlab.com/DangerApps/blkdrop</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>BSD 3-Clause License</td></tr>
</table>
